module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/essential'],
  rules: {
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-console': 'off',
    // 'import/no-extraneous-dependencies': [
    //   'error',
    //   {
    //     devDependencies: false,
    //     optionalDependencies: false,
    //     peerDependencies: false,
    //   },
    // ],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
};

// module.exports = {
//   root: true,
//   env: {
//     node: true,
//   },
//   extends: ['plugin:vue/essential'],
//   rules: {
//     'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
//     'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
//     'linebreak-style': [
//       'error',
//       process.platform === 'win32' ? 'windows' : 'unix',
//     ],
//     'implicit-arrow-linebreak': ['error', 'below'],
//     'brace-style': ['error', 'allman', { allowSingleLine: true }],
//     // 'import/no-extraneous-dependencies': [
//     //   'error',
//     //   {
//     //     devDependencies: false,
//     //     optionalDependencies: false,
//     //     peerDependencies: false,
//     //     packageDir: __dirname,
//     //   },
//     // ],
//   },
//   parserOptions: {
//     parser: 'babel-eslint',
//   },
// };

// module.exports = {
//   root: true,
//   env: {
//     node: true,
//   },
//   extends: ['plugin:vue/essential'],
//   parserOptions: {
//     parser: 'babel-eslint',
//   },
//   rules: {
//     'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
//     'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
//     'import/no-extraneous-dependencies': [
//       'error',
//       {
//         devDependencies: false,
//         optionalDependencies: false,
//         peerDependencies: false,
//       },
//     ],
//   },
// };
