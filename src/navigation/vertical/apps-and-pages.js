export default [
  // {
  //   title: 'Products',
  //   route: 'apps-products-list',
  //   icon: 'GridIcon'
  // },
  // {
  //   title: 'Orders',
  //   route: 'apps-orders-list',
  //   icon: 'ShoppingCartIcon'
  // },
  {
    title: 'Categories',
    route: 'apps-categories',
    icon: 'ListIcon',
  },
  {
    title: 'Sub Categories',
    route: 'apps-subcategories',
    icon: 'ListIcon',
  },
  {
    title: 'Occasions',
    route: 'apps-occasions',
    icon: 'ClipboardIcon',
  },
  {
    title: 'Banners',
    route: 'apps-banners',
    icon: 'TvIcon',
  },
  // {
  //   title: 'Pickup Slots',
  //   route: 'apps-pickup',
  //   icon: 'ClipboardIcon',
  // },
  {
    title: 'Promo Code',
    route: 'apps-promocode',
    icon: 'StarIcon',
  },
  {
    title: 'Vendors',
    route: 'apps-all-vendors',
    icon: 'UsersIcon',
  },

  {
    title: 'Products',
    route: 'apps-all-products',
    icon: 'GridIcon',
  },
  {
    title: 'Orders',
    route: 'apps-all-orders',
    icon: 'ShoppingBagIcon',
  },
  {
    title: 'Messages',
    route: 'apps-chat',
    icon: 'MessageSquareIcon',
  },
  
  // {
  //   title: 'Seller Registration',
  //   route: null,
  //   icon: 'UserPlusIcon',
  //   action:'read',
  //   resourse:''
  // },
  // {
  //   title: 'Reports',
  //   route: 'apps-todo',
  //   icon: 'BarChart2Icon',
  // },
  // {
  //   title: 'Setting',
  //   route: '#',
  //   icon: 'SettingsIcon',
  // },

]
